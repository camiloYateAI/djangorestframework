
from rest_framework import serializers
from .models import Article



class ArticleSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    autor = serializers.CharField(max_length=100)
    email = serializers.EmailField(max_length=100)
    date  =  serializers.DateTimeField()

    def create(self, validated_data):
        return Article.objects.create(validated_data)

    def update(self, instance, validated_data):
        instance.ttle = validated_data.get('title', instance.ttle)
        instance.autor = validated_data.get('autor', instance.autor)
        instance.email = validated_data.get('title', instance.email)
        instance.email = validated_data.get('title', instance.email)
        instance.save()
        return instance